#!/usr/bin/env python

# This code is taken from https://github.com/appscodelabs/libbuild/blob/master/docker.py
# Thanks to @tamalsaha (https://github.com/tamalsaha)

import base64
import json
import sys
from os.path import expanduser

from urllib.parse import urlencode, unquote, urlparse
from urllib.request import urlopen, Request, build_opener, HTTPHandler
from urllib.error import HTTPError


def read_json(name):
    try:
        with open(name, 'r') as f:
            return json.load(f)
    except IOError as err:
        print(err)
        sys.exit(1)

# Authenticates the user on docker hub by reading the credentials from the config file
def authenticate():
    home = expanduser("~")
    config = read_json(home + "/.docker/config.json")
    s = config['auths']['https://index.docker.io/v1/']['auth']

    if sys.version_info[0] >= 3:
        s = bytes(s, 'utf-8')
        credentials = base64.b64decode(s).decode('utf-8')
    else:
        credentials = base64.b64decode(s)

    cred = credentials.split(':')
    username = cred[0]
    password = cred[1]

    url = 'https://hub.docker.com/v2/users/login/'
    data = urlencode({'username': username, 'password': password}).encode("utf-8") # ref: https://stackoverflow.com/questions/30760728/python-3-urllib-produces-typeerror-post-data-should-be-bytes-or-an-iterable-of
    req = Request(url, data)
    response = urlopen(req)
    try:
        body = response.read().decode('utf-8')
        body = json.loads(body)
        return body['token']
    except:
        print("Error obtaining token")
        sys.exit(1)

# ref: https://success.docker.com/Cloud/Solve/How_do_I_authenticate_with_the_V2_API%3F
def del_tag(ciRegistryImage, tag):
    namespace, repo = _parse_ci_url(ciRegistryImage)
    token = authenticate()
    try:
        _del_tag(namespace, repo, tag, token)
        sys.exit(0)
    except HTTPError:
        print("Exiting with failure state")
        sys.exit(1)

def del_tags(ciRegistryImage):
    namespace, repo = _parse_ci_url(ciRegistryImage)
    token = authenticate()
    for line in sys.stdin:
        try:
            _del_tag(namespace, repo, line.rstrip(), token)
        except HTTPError:
            pass
    sys.exit(0)

def help():
    print('docker.py del_tag <CI_REGISTRY_IMAGE> <tag>')
    print("OR")
    print("cat list_of_tags.txt | docker.py del_tags <CI_REGISTRY_IMAGE>")

def _del_tag(namespace, repo, tag, token):
    print(('Commence to delete %s/%s:%s ...' % (namespace, repo, tag)))
    url = 'https://hub.docker.com/v2/repositories/%s/%s/tags/%s/' % (namespace, repo, tag)
    headers = {
        'Authorization': 'JWT %s' % token
    }
    request = Request(url=url, headers=headers)
    request.get_method = lambda: 'DELETE'
    try:
        opener = build_opener(HTTPHandler)
        opener.open(request)
        print(('%s/%s:%s deleted successfully.' % (namespace, repo, tag)))
        # body = response.read().decode('utf-8')
    # If we have an HTTPError, try to follow the response
    except HTTPError as err:
        print(("Failed to delete tag. Error: %s" % err))
        raise

def _parse_ci_url(ciRegistryImage):
    parts = ciRegistryImage.split("/")
    return [parts[-2], parts[-1]]

if __name__ == "__main__":
    if len(sys.argv) > 1:
        # http://stackoverflow.com/a/834451
        # http://stackoverflow.com/a/817296
        globals()[sys.argv[1]](*sys.argv[2:])
    else:
        help()
