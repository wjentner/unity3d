# Python script to delete tags from docker hub

Prerequisites:
1. python3 is required (not tested with python2)
2. you need to be logged in via docker (docker login ...) as the script will read your authentication information from ~/.docker/config.json

## Delete a specific tag

In the CI, the script is being called like this:
```
python3 ./ci-utils/docker.py del_tag "$CI_REGISTRY_IMAGE" "$TAG-$CI_COMMIT_REF_SLUG"
```

which translates to (example values):
```
python3 ./ci-utils/docker.py del_tag "index.docker.io/gableroux/unity3d" "2018.3.7f1-branch-name"
```

## Delete a list of tags

Given an input where each tag occurs in a line, for example:
```
tag1
tag2
tag3
```

the python script can be called as follows to delete all these tags:
```
cat tags.txt | python3 ./ci-utils/docker.py del_tags "index.docker.io/gableroux/unity3d"
```

Note the different function name `del_tags`.
If the tag is not available an error will be printed but the script will continue deleting the other tags.